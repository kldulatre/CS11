#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <stdbool.h>

void num1(void){
	char str[10];
	int counter,zero=0,pair=0;
	printf("Enter number: ");
	scanf("%s",str);
	printf("%s! has ",str);
	counter=atoi(str)+1;
	while(counter!=0){
		counter--;
		if(counter==0) break;
		sprintf(str,"%d",counter);
		switch(str[strlen(str)-1]-'0'){
			case 0:
				zero++;
				break;
			case 5:
				pair++;
				if(pair==2){
					pair=0;
					zero++;
				}
				break;
			case 2:
				pair++;
				if(pair==2){
					pair=0;
					zero++;
				}
				break;
		}
	}
	printf("%i rightmost 0's\n",zero);
	return;
}

void num2(void){
	int x1,y1,x2,y2,x3,y3,x4,y4;
	int collector[100],x=0,y,counter;
	char temp[10];
	char container[100];
	while(true){
		printf("Enter x1,y1,x2,y2,x3,y3,x4,y4\n");
		printf("Separate each input by a comma: ");
		scanf("%s",container);
		int index,pass=0;
		for(index=0,counter=0,pass=0,y=0;index<=strlen(container);index++){
			switch(container[index]){
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case '0':
				case '-':
					temp[x] = container[index];
					x++;
					continue;
				case ',':
				case '\0':
					if(atoi(temp)!=0||temp[0]=='0'){
						temp[x]='\0';
						x=0;
						counter++;
						collector[y]=atoi(temp);
						y++;
						temp[x]='k';
						continue;
					}
					else{
						pass=1;
						break;
					}
				default:
					pass=1;
					break;	
			}
		}
		if(pass==1||counter!=8){
			printf("\nPlease input properly!\n");
			continue;
		}
		break;
	}
	for(x=0;x<8;x++)
	x1 = collector[0];
	y1 = collector[1];
	x2 = collector[2];
	y2 = collector[3];
	x3 = collector[4];
	y3 = collector[5];
	x4 = collector[6];
	y4 = collector[7];
	float m1,m2;
	m1  = (y2-y1)/(x2-x1);
	m2  = (y4-y3)/(x4-x3);
	if(m1==m2) printf("NO INTERSECTION!");
	else{
		float xint,yint;
		xint = (m1*x1-y1-m2*x3+y3)/(m1-m2);
		yint = m1*(xint-x1)+y1;
		printf("(%f,%f)\n",xint,yint);
	}
	return;
}

int primeFinder(int number){
	float limit;
	int y;
	limit = sqrt(number);
	for(y=2;y<=limit;y++){
		if(number%y==0){
			return 1;		
		}	
	}
	return 0;
}

void num3(void){
	int x,n,temp;
	int key[100],count[100];
	int a=0;
	printf("Enter number: ");
	scanf("%i",&n);
	temp = n;
	key[a] = 1;
	count[a] = 1;
	for(x=2;x<=sqrt(temp)&&n>x;x++){
		if(primeFinder(x)==0){
			while(n%x==0){
				n = n/x;
				if(key[a]!=x){
					a++;
					count[a]=0;
				}
				key[a] = x;
				count[a] = count[a] + 1;
			}
		}
	}
	int z,pass=1;
	printf("%i =",temp);
	int product=1;
	for(z=1;z<a+1;z++){
		if(pass==1){
			printf(" %i^%i",key[z],count[z]);
			pass=0;
		}
		else printf(" x %i^%i",key[z],count[z]);
		product = product*pow(key[z],count[z]);
	}
	printf(" x %i^%i",n,1);
	printf("\n");
	return;
}

struct node5{
	int coe;
	int pos;
	struct node5* next;	
};

void num5(void){
	char fPoly[50],sPoly[50];
	struct node5 *fCon=NULL,*fHead=NULL,*sCon=NULL,*sHead=NULL;

	printf("Enter 1st polynomial: ");
	scanf(" %s",fPoly);
	printf("Enter 2nd polynomial: ");
	scanf(" %s",sPoly);
	
	char *token1;
	int counter1 = 0;
	token1 = strtok(fPoly,",");
	while(token1!=NULL){
		fCon = (struct node5 *)malloc(sizeof(struct node5));
		fCon->coe = atoi(token1);
		counter1++;
		fCon->pos = counter1;
		fCon->next = fHead;
		fHead = fCon;
		token1 = strtok(NULL,",");
	}

	char *token2;
	int counter2 = 0;
	token2 = strtok(sPoly,",");
	while(token2!=NULL){
		sCon = (struct node5 *)malloc(sizeof(struct node5));
		sCon->coe = atoi(token2);
		counter2++;
		sCon->pos = counter2;
		sCon->next = sHead;
		sHead = sCon;
		token2 = strtok(NULL,",");
	}

	struct node5 *pCon=NULL,*pHead=NULL;

	int max=0;

	while(fCon){
		while(sCon){
			pCon = (struct node5 *)malloc(sizeof(struct node5));
			pCon->coe = fCon->coe*sCon->coe;
			if(max<counter1-fCon->pos+counter2-sCon->pos) max = counter1-fCon->pos+counter2-sCon->pos; 
			pCon->pos = counter1-fCon->pos+counter2-sCon->pos;
			pCon->next = pHead;
			pHead = pCon;
			sCon = sCon->next;
		}
		sCon = sHead;
		fCon = fCon->next;
	}
	fCon = fHead;

	int temp = 0,add = 0;
	int pass=max;

	while(max!=-1){
		while(pCon){
			if(max==pCon->pos){
				add += pCon->coe;
			}
			pCon = pCon->next;
		}
		if(pass==max) printf("%i^%i ",add,max);
		else if(max==0) printf("+ %i",add);
		else printf("+ %i^%i ",add,max);
		
		add = 0;
		pCon = pHead;
		max--;
	}

	printf("\n");

	free(fCon);
	free(sCon);

	return;
}

void num6(void){
	int n,i;
	printf("Enter n: ");
	scanf("%i",&n);
	printf("Enter i: ");
	scanf("%i",&i);
	
	int x,binary=0;

	for(x=1;x<=i;x++){
		if(n%x==0){
			if(binary==0)
				binary=1;
			else
				binary=0;
		}
	}

	printf("Binary: %i\n",binary);

	return;
}

int factorChecker(int num1,int num2){
	if(num1>num2){
		int temp=num2;
		num2 = num1;
		num1 = temp;
	}
	int x;
	for(x=2;x<=num1;x++){
		if((num1%x==0)&&(num2%x==0)){
			return 0;
		}
	}
	return 1;
}

void num7(void){
	int n;
	printf("Enter n: ");
	scanf("%i",&n);
	int x;
	for(x=1;x<n;x++){
		int ans = factorChecker(x,n);
		if(ans==1)
			printf("%i/%i,",x,n);
	}
	printf("\n");
	return;
}

int getDistance(int x1,int y1,int x2,int y2){
	return sqrt(pow(x2-x1,2)+pow(y2-y1,2));
}

void num8(void){
	char xcontainer[100],xtemp[10];
	int xcollector[100];
	int x=0,y,counter;
	printf("Enter x-components(then press enter): ");
	while(true){
		scanf("%s",xcontainer);
		int index,pass=0;
		for(index=0,counter=0,pass=0,y=0;index<=strlen(xcontainer);index++){
			switch(xcontainer[index]){
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case '0':
				case '-':
					xtemp[x] = xcontainer[index];
					x++;
					continue;
				case ',':
				case '\0':
					if(atoi(xtemp)!=0||xtemp[0]=='0'){
						xtemp[x]='\0';
						x=0;
						counter++;
						xcollector[y]=atoi(xtemp);
						y++;
						xtemp[x]='k';
						continue;
					}
					else{
						pass=1;
						break;
					}
				default:
					pass=1;
					break;	
			}
		}
		break;
	}
	int tempCounter = counter;
	char ycontainer[100],ytemp[10];
	int ycollector[100];
	printf("Enter y-components(then press enter): ");
	while(true){
		scanf("%s",ycontainer);
		int index,pass=0;
		for(index=0,counter=0,pass=0,y=0;index<=strlen(ycontainer);index++){
			switch(ycontainer[index]){
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
				case '0':
				case '-':
					ytemp[x] = ycontainer[index];
					x++;
					continue;
				case ',':
				case '\0':
					if(atoi(ytemp)!=0||ytemp[0]=='0'){
						ytemp[x]='\0';
						x=0;
						counter++;
						ycollector[y]=atoi(ytemp);
						y++;
						ytemp[x]='k';
						continue;
					}
					else{
						pass=1;
						break;
					}
				default:
					pass=1;
					break;	
			}
		}
		if(pass==1||counter!=tempCounter){
			printf("Enter y-components(then press enter): ");
			continue;
		}
		break;
	}
	int a=0,b=0,index1=0,index2=0;
	int min = getDistance(xcollector[a],ycollector[a],xcollector[a++],ycollector[a++]);
	for(a=0;a<tempCounter;a++){
		for(b=a;b<tempCounter;b++){
			if(a==b){
				continue;
			}
			else{
				int temp = getDistance(xcollector[a],ycollector[a],xcollector[b],ycollector[b]);
				if(temp<min){
					min = temp;
					index1 = a;
					index2 = b;
				}
			}
		}
	}
	printf("Shortest Distance of (%i,%i) and (%i,%i) is %i\n",xcollector[index1],ycollector[index1],xcollector[index2],ycollector[index2],min);
	return;
}

long long int combination(int n,int r){
	if(r==0||n==r){
		return 1;
	}
	else{
		long long int ans=n,x,den=r;
		for(x=n-1;x>(n-r);x--){
			ans *= x;
		}
		for(x=r-1;x>1;x--){
			den *= x;
		}
		return ans/den;
	}
}

void num9(void){
	int n;
	printf("Enter n: ");
	scanf("%i",&n);
	int x;
	for(x=0;x<=n;x++){
		printf("%llu ",combination(n,x));
	}
	printf("\n");
	return;
}

void num10(void){
	int n,x;
	printf("Enter a number: ");
	scanf("%i",&n);
	for(x=n/1000;x>0;x--){
		printf("M");
		n -= 1000;
	}
	for(x=n/500;x>0;x--){
		printf("D");
		n -= 500;
	}
	for(x=n/100;x>0;x--){
		printf("C");
		n -= 100;
	}
	for(x=n/50;x>0;x--){
		printf("L");
		n -= 50;
	}
	for(x=n/10;x>0;x--){
		printf("X");
		n -= 10;
	}
	for(x=n/5;x>0;x--){
		printf("V");
		n -= 5;
	}
	for(x=n/1;x>0;x--){
		printf("I");
		n -= 1;
	}
	printf("\n");
	return;
}

void frac(int top,int bot){
	printf(" %i/%i",top,bot);
}

void subtract(int *top1,int *bot1,int top2,int bot2){
	int top = *top1*bot2 - top2**bot1;
	int bot = *bot1*bot2;
	int x;
	for(x=2;x<=top||x<=bot;x++){
		if((top%x==0)&&(bot%x==0)){
			top = top/x;
			bot = bot/x;
			x=2;
		}
	}
	*top1 = top;
	*bot1 = bot;
}


void num11(void){
	int top,bot,origTop,origBot;
	printf("Enter numerator: ");
	scanf("%i",&top);
	printf("Enter denominator: ");
	scanf("%i",&bot);

	origTop = top;
	origBot = bot;

	int x,pass=1,min=1;
	while(min<1000){
		for(x=min+1;top!=1&&x<1001;x++){
			if(x>1000||top/bot<1/1000){
				printf(" end");
				break;
			}
			int tempTop=top,tempBot=bot;
			if(top*x>bot)subtract(&top,&bot,1,x);
			if(tempTop!=top,tempBot!=bot){
				if(pass==1){ 
					min = x;
					pass=0;
				}
				frac(1,x);
				if(top==1){
					frac(top,bot);
				}
				if(x>1000||top/bot<1/1000){
					printf(" end");
					break;
				}
				tempTop=top;
				tempBot=top;
			}
		}
		pass = 1;
		top = origTop;
		bot = origBot;
		printf("\n");
	}

	return;
}

void num12(void){
	int n,x,plus=1;
	printf("Enter Denomination: ");
	scanf("%i",&n);
	printf("%i = ",n);
	if((x=n/1000)>=1){ 
		printf("(%i) 1000",x);
		plus = 0;
		n -= x*1000;
	}
	if((x=n/500)>=1){
		if(plus==0) printf(" + (%i) 500",x);
		else{
			printf("(%i) 500",x);
			plus = 0;
		}
		n -= x*500;
	}
	if((x=n/200)>=1){ 
		if(plus==0) printf(" + (%i) 200",x);
		else{
			printf("(%i) 200",x);
			plus = 0;
		}
		n -= x*200;
	}
	if((x=n/100)>=1){ 
		if(plus==0) printf(" + (%i) 100",x);
		else{
			printf("(%i) 100",x);
			plus = 0;
		}
		n -= x*100;
	}
	if((x=n/50)>=1){ 
		if(plus==0) printf(" + (%i) 50",x);
		else{
			printf("(%i) 50",x);
			plus = 0;
		}
		n -= x*50;
	}
	if((x=n/20)>=1){ 
		if(plus==0) printf(" + (%i) 20",x);
		else{
			printf("(%i) 20",x);
			plus = 0;	
		}
		n -= x*20;
	}
	if((x=n/10)>=1){ 
		if(plus==0)printf(" + (%i) 10",x);
		else{
			printf("(%i) 10",x);
			plus = 0;
		}
		n -= x*10;
	}
	if((x=n/5)>=1){ 
		if(plus==0) printf(" + (%i) 5",x);
		else{
			printf("(%i) 5",x);
			plus = 0;	
		}
		n -= x*5;
	}
	if((x=n/1)>=1){ 
		if(plus==0) printf(" + (%i) 1",x);
		else{
			printf("(%i) 1",x);
			plus = 0;
		}
		n -= x*1;
	}
	printf("\n");
	return;
}

struct num13{
	int num1;
	int num2;
	int sum;
	struct num13 *next;
};

struct ans13{
	int ans;
	struct ans13 *next;
};

void num13(void){
	char num1[100],num2[100];

	printf("Enter num1: ");
	fgets(num1,100,stdin);
	printf("Enter num2: ");
	fgets(num2,100,stdin);
	if(strlen(num1)>strlen(num2)){
		int x = strlen(num1)-strlen(num2)+1;
		while(x>0){
			char d[100]="0";
			strcpy(num2,strcat(d,num2));
			x--;
		}
	}
	else if(strlen(num1)<strlen(num2)){
		int x = strlen(num2)-strlen(num1);
		while(x>0){
			char d[100]="0";
			strcpy(num1,strcat(d,num1));
			x--;
		}
	}
	else;
	int x=0;
	struct num13 *head=NULL,*current;
	while(num2[x]!='\n'||num1[x]!='\n'){
		current = malloc(sizeof(struct num13));
		current->num1 = num1[x]-'0';
		current->num2 = num2[x]-'0';
		current->next = head;
		head = current;
		x++;
	}
	int excess = 0;
	while(current){
		if(current->num1+current->num2+excess>=10){
			current->sum=current->num1+current->num2+excess-10;
			excess=1;
		}
		else{
			current->sum=current->num1+current->num2+excess;
			excess = 0;
		}
		current = current->next;
	}
	current=head;
	struct ans13 *myAns,*myHead=NULL;
	while(current){
		myAns = malloc(sizeof(struct ans13));
		myAns->ans = current->sum;
		myAns->next = myHead;
		myHead = myAns;
		current = current->next;
	}
	printf("answer: ");
	if(excess==1) printf("1");
	while(myAns){
		printf("%i",myAns->ans);
		myAns = myAns->next;
	}
	return;
}

void convertToBinary(int num){
  int newNum=0;
  int counter=0;
  while(num!=0){
    newNum = newNum+(num%2)*pow(10,counter);
    num = num/2;
    counter++;
  }
  printf("Binary: %i\n",newNum);
  return;
}

void convertToOctal(int num){
  int newNum=0;
  int counter=0;
  while(num!=0){
    newNum = newNum+(num%8)*pow(10,counter);
    num = num/8;
    counter++;
  }
  printf("Octal: %i\n",newNum);
  return;
}

void convertToHexadical(int num){
  char str[50];
  int counter=0;
  while(num!=0){
    if(num%16<=9)
      str[counter] = num%16+'0';
    else{
      switch(num%16){
        case 10:
          str[counter] = 'a';
          break;
        case 11:
          str[counter] = 'b';
          break;
        case 12:
          str[counter] = 'c';
          break;
        case 13:
          str[counter] = 'd';
          break;
        case 14:
          str[counter] = 'e';
          break;
        case 15:
          str[counter] = 'f';
          break;
      }
    }
    num = num/16;
    counter++;
  }
  printf("Hexadecimal: ");
  while(counter>=0){
    printf("%c",str[counter]);
    counter--;
  }
  printf("\n");
  return;
}

void num14(void)
{
    int num,base;
    printf("Enter a Number: ");
    scanf(" %i",&num);
    printf("Enter the Base: ");
    scanf(" %i",&base);
    switch(base){
      case 2:
        convertToBinary(num);
        break;
      case 8:
        convertToOctal(num);
        break;
      case 16:
        convertToHexadical(num);
        break;
    }
    return;
}

struct num15{
	int number;
 	int finish;
 	struct num15* next;
};

void numm15(void){
	struct num15 *current=NULL, *head=NULL;
	char str[100];
	char *token;
	int countLeft=0;

	printf("Enter: ");
	scanf("%s",str);

	token = strtok(str,",");
	while(token){
		countLeft++;
		current = malloc(sizeof(struct num15));
		current->number = atoi(token);
		current->next = head;
		head = current;
		token = strtok(NULL,",");
	}

	int min=0,pass=0;
	while(countLeft!=0){
		while(current){
			if(current->finish!=1){
				min = current->number;
				break;
			}
			current = current->next;
		}
		current = head;

		while(current){
			if(current->number<min&&current->finish!=1){
				min = current->number;
			}
			current = current->next;
		}
		current = head;

		while(current){
			if(current->number==min){
				if(pass!=0) printf(",%i",min);
				else{ 
					printf("%i",min);
					pass = 1;
				}
				current->finish = 1;
				countLeft--;
			}
			current = current->next;
		}
		current = head;
	}

	printf("\n");

	return;
}

struct Circle{
	int r;
	int y;
	int x;
	struct Circle *next;
};

struct temp{
	int x;
	struct temp *next;
};

int intersectCircle(int x1,int x2,int y1,int y2,int r1,int r2){
	int pass=0;
	if(pow(r1+r2,2)>=pow(x1-x2,2)+pow(y1-y2,2)&&pow(x1-x2,2)+pow(y1-y2,2)>=pow(r1-r2,2)) pass=1;
	return pass;
}

void num16(void){
	struct Circle *current,*current2,*head=NULL;
	struct temp *tem,*tempHead=NULL;
	int counter = 1;
	char str[256];

	printf("Enter x: ");
	fgets(str,256,stdin);
	str[strlen(str)-1] = '\0';
	char* token= strtok(str, ",");
	while (token) {
	    tem = (struct temp *)malloc(sizeof(struct temp));
	    tem->x = atoi(token);
	    tem->next = tempHead;
	    tempHead = tem;
	    token = strtok(NULL, ",");
	}
	while(tem){
		current = (struct Circle *)malloc(sizeof(struct Circle));
		current->x = tem->x;
		current->next = head;
		head = current;
		tem = tem->next;
	}

	printf("Enter y: ");
	fgets(str,256,stdin);
	str[strlen(str)-1] = '\0';
	token= strtok(str, ",");
	while(current){
		current->y = atoi(token);
		current = current->next;
		token = strtok(NULL, ",");
	}
	current = head;

	printf("Enter r: ");
	fgets(str,256,stdin);
	str[strlen(str)-1] = '\0';
	token= strtok(str, ",");
	while(current){
		current->r = atoi(token);
		current = current->next;
		token = strtok(NULL, ",");
	}
	current = head;

	current2 = current;

	int max = 0;
	int great = 0,gx,gy,gr;

	while(current){
		while(current2){
			int pass = intersectCircle(current->x,current2->x,current->y,current2->y,current->r,current2->r);
			if(pass==1){
				max++;
			}
			current2 = current2->next; 
		}
		current2 = head;
		if(great<max){
			great = max;
			gx = current->x;
			gy = current->y;
			gr = current->r;
		}
		max = 0;
		current = current->next;
	}
	printf("(%i,%i,%i) with %i intersection to other circles\n",gx,gy,gr,great-1);

	free(current);
	free(head);
	free(tem);
	free(tempHead);
	return;
}

void num17(void){
	struct Circle *current,*head=NULL;
	struct temp *tem,*tempHead=NULL;
	int counter = 1;
	char str[256];

	printf("Enter x: ");
	fgets(str,256,stdin);
	str[strlen(str)-1] = '\0';
	char* token= strtok(str, ",");
	while (token) {
	    tem = (struct temp *)malloc(sizeof(struct temp));
	    tem->x = atoi(token);
	    tem->next = tempHead;
	    tempHead = tem;
	    token = strtok(NULL, ",");
	}
	while(tem){
		current = (struct Circle *)malloc(sizeof(struct Circle));
		current->x = tem->x;
		current->next = head;
		head = current;
		tem = tem->next;
	}

	printf("Enter y: ");
	fgets(str,256,stdin);
	str[strlen(str)-1] = '\0';
	token= strtok(str, ",");
	while(current){
		current->y = atoi(token);
		current = current->next;
		token = strtok(NULL, ",");
	}
	current = head;

	printf("Enter r: ");
	fgets(str,256,stdin);
	str[strlen(str)-1] = '\0';
	token= strtok(str, ",");
	while(current){
		current->r = atoi(token);
		current = current->next;
		token = strtok(NULL, ",");
	}
	current = head;

	float x1,x2,xf,y1,y2,yf,r1,r2,rf,d;

	x1 = current->x;
	y1 = current->y;
	r1 = current->r;
	current = current->next;

	while(current){

		x2 = current->x;
		y2 = current->y;
		r2 = current->r;

		if(r2>r1){
			float temp;

			temp = r2;
			r2 = r1;
			r1 = temp;
			
			temp = x2;
			x2 = x1;
			x1 = temp;
			
			temp = y2;
			y2 = y1;
			y1 = temp;
		}

		d = sqrt(pow(x2-x1,2)+pow(y2-y1,2));

		if((r1+r2+d)>2*r1){
			rf = (d+r1+r2)/2;
			xf = (x1+x2)/2;
			yf = (y1+y2)/2;
			x1 = xf;
			y1 = yf;
			r1 = rf;
		}

		current = current->next;
	}
	current = head;

	printf("\n(%f,%f,%f)\n",x1,y1,r1);
	
	free(current);
	// free(head);
	free(tem);
	free(tempHead);

	return;
}

void getKeys(char l){
	if(l=='a') printf("2");
	else if(l=='b') printf("22");
	else if(l=='c') printf("222"); 
	else if(l=='d') printf("3"); 
	else if(l=='e') printf("33"); 
	else if(l=='f') printf("333"); 
	else if(l=='g') printf("4"); 
	else if(l=='h') printf("44"); 
	else if(l=='i') printf("444"); 
	else if(l=='j') printf("5"); 
	else if(l=='k') printf("55"); 
	else if(l=='l') printf("555"); 
	else if(l=='m') printf("6"); 
	else if(l=='n') printf("66"); 
	else if(l=='o') printf("666"); 
	else if(l=='p') printf("7"); 
	else if(l=='q') printf("77"); 
	else if(l=='r') printf("777"); 
	else if(l=='s') printf("7777");
	else if(l=='t') printf("8"); 
	else if(l=='u') printf("88"); 
	else if(l=='v') printf("888"); 
	else if(l=='w') printf("9"); 
	else if(l=='x') printf("99"); 
	else if(l=='y') printf("999"); 
	else if(l=='z') printf("9999");
	else if(l=='\n') printf("\n");
	else printf("0"); 
}

void num18(void){
	char str[1000];
	printf("Enter String: ");
	scanf(" %s",str);
	int len = strlen(str),x;
	for(x=0;x<len;x++) getKeys(str[x]);
	return;
}

void num19(void) 
{
	char string[100];
	int sched[100], current, i, j, k;
	printf("Enter schedule");
	
	fgets(string,100,stdin);
	
	i = 0;
	j = 0;
	while (i < strlen(string))
	{
	    if ( isdigit(string[i]) &&  string[i + 1] == ':' && isdigit(string[i + 2]) && isdigit(string[i + 3]) && string[i + 4] == 'p' )
	    {
	        int a = string[i], b = string[i + 2], c = string[i + 3];
	        current = (a - 48)*100 + (b - 48)*10 + (c - 48) + 1200;
	        sched[j] = current;
	        j++;
	        i = i + 5;
	    }
	    else if ( isdigit(string[i]) &&  string[i + 1] == ':' && isdigit(string[i + 2]) && isdigit(string[i + 3]))
	    {
	        int a = string[i], b = string[i + 2], c = string[i + 3];
	        current = (a - 48)*100 + (b - 48)*10 + (c - 48);
	        sched[j] = current;
	        j++;
	        i = i + 4;
	    }
	    else if ( isdigit(string[i]) && isdigit(string[i + 1]) > 0 && string[i + 2] == ':' && isdigit(string[i + 3]) && isdigit(string[i + 4]))
	    {
	        int a = string[i], b = string[i + 1], c = string[i + 3], d = string[i + 4];
	        current = (a - 48)*1000 + (b - 48)*100 + (c - 48)*10 + (d - 48);
	        sched[j] = current;
	        j++;
	        i = i + 5;
	    }
	    else if ( isdigit(string[i]) && isdigit(string[i + 1]) > 0 && isdigit(string[i + 2]) > 0 && isdigit(string[i + 3]))
	    {
	        int a = string[i], b = string[i + 1], c = string[i + 2], d = string[i + 3];
	        current = (a - 48)*1000 + (b - 48)*100 + (c - 48)*10 + (d - 48);
	        sched[j] = current;
	        j++;
	        i = i + 4;
	    }
	   else if ( isdigit(string[i]) && isdigit(string[i + 1]) == 0 && isdigit(string[i + 2]) == 0 && string[i + 3] == 'p')
	    {
	        int a = string[i], b = string[i + 1], c = string[i + 2];
	        current = (a - 48)*100 + (b - 48)*10 + (c - 48) + 1200;
	        sched[j] = current;
	        j++;
	        i = i + 4;
	    }
	    else if ( isdigit(string[i]) && isdigit(string[i + 1]) > 0 && isdigit(string[i + 2]) > 0 && string[i + 3] == 'p')
	    {
	        int a = string[i], b = string[i + 1], c = string[i + 2];
	        current = (a - 48)*100 + (b - 48)*10 + (c - 48) + 1200;
	        sched[j] = current;
	        j++;
	        i = i + 4;
	    }
	    else if ( isdigit(string[i]) && isdigit(string[i + 1]) > 0 && isdigit(string[i + 2]) > 0)
	    {
	        int a = string[i], b = string[i + 1], c = string[i + 2];
	        current = (a - 48)*100 + (b - 48)*10 + (c - 48);
	        sched[j] = current;
	        j++;
	        i = i + 3;
	    }
	    
	     else if ( isdigit(string[i]) && isdigit(string[i + 1]) > 0)
	    {
	        int a = string[i], b = string[i + 1];
	        current = (a - 48)*1000 + (b - 48)*100;
	        sched[j] = current;
	        j++;
	        i = i + 2;
	    }
	    else if ( isdigit(string[i]) && string[i + 1] == 'p')
	    {
	        int a = string[i];
	        current = (a - 48)*100 + 1200;
	        sched[j] = current;
	        j++;
	        i = i + 2;
	    }
	    else if ( isdigit(string[i]) )
	    {
	        int a = string[i];
	        current = (a - 48)*100;
	        sched[j] = current;
	        j++;
	        i++;
	    }
	    else
	    {
	        i++;
	    }
	}
	k = 0;
	while (k < j)
	{
	    i = 0;
	    while(i < (j - 1))
    	{
	        if (sched[i] > sched[i + 1])
	        {
	             int mem;
	             mem = sched[i];
	             sched[i] = sched[i + 1];
	             sched[i + 1] = mem;
	        }
	         i++;
    	}
    	k++;
	}
	
	
	i =1;
	while (i < j - 1)
	{
	    if (sched[i] >= 1300 && 1300 <= sched[i + 1])
	    {
	        printf("%dp - %dp, ", sched[i] - 1200, sched[i+ 1] - 1200);
	        i = i + 2;
	    }
	    else if (sched[i] >= 1200 && sched[i + 1] >= 1300)
	    {
	        printf("%dp - %dp, ", sched[i], sched[i + 1] - 1200);
	        i = i + 2;
	    }
	    else if (sched[i] >= 1200 && sched[i + 1] >= 1200)
	    {
	        printf("%dp - %dp, ", sched[i], sched[i + 1]);
	        i = i + 2;
	    }
	    else if (sched[i + 1] >= 1300)
	    {
	        printf("%da - %dp, ", sched[i], sched[i + 1] - 1200);
	        i = i + 2;
	    }
	    else if (sched[i + 1] >= 1200)
	    {
	        printf("%da - %dp, ", sched[i], sched[i + 1]);
	        i = i + 2;
	    }
	    else
	    {
	        printf("%da - %da, ", sched[i], sched[i + 1]);
	        i = i + 2;
	    }
	}
	
	if (sched[j - 1] != 2000)
	{
	    if (sched[j - 1] >= 1300)
	    {
	        printf("%dp - 8p", sched[j - 1] - 1200);
	    }
	    else if (sched[j - 1] >= 1200)
	    {
	        printf("%dp - 8p", sched[j - 1]);
	    }
	    else
	    {
	        printf("%da - 800p", sched[j - 1]);
	    }
	}
	printf("\n");
	return;
}

struct storage{
	char word[100];
	int count;
	struct storage *next;
};

void num20(void){
	char str[1000];
	FILE *fp;
	struct storage *current=NULL, *head=NULL;

	fp = fopen("20.txt","w");
	printf("Enter string: ");
	fgets(str,1000,stdin);
	fputs(str,fp);
	fclose(fp);
	fp = fopen("20.txt","r");
	char c;
	do{
		c = fscanf(fp,"%s",str);
		if(c!=EOF){
			int pass = 0;
			while(current){
				if(strcmp(current->word,str)==0){
					current->count = current->count + 1;
					pass = 1;
					current = head;
					break;
				}
				current = current->next;
			}
			if(pass==0){
				current =malloc(sizeof(struct storage));
				strcpy(current->word,str);
				current->count = 1;
				current->next = head;
				head = current;
			}
		}
	}while(c!=EOF);

	while(current){
		if(current->next)
			printf("%s %i, ",current->word,current->count);
		else
			printf("%s %i ",current->word,current->count);
		current = current->next;
	}

	return;
}

int main(void){
	while(true){
		printf("\n\nCS 11 Project Menu: \n");
		printf("\t1 Rightmost zoroes\n");
		printf("\t2 Intersection of line\n");
		printf("\t3 Prime Factorization\n");
		printf("\t4 Subset\n");
		printf("\t5 Product of two polynomials\n");
		printf("\t6 Mabu\n");
		printf("\t7 Fraction less than 1\n");
		printf("\t8 Shortest Distance\n");
		printf("\t9 Pascals Triangle\n");
		printf("\t10 Roman Numerals\n");
		printf("\t11 Egyptian Fraction\n");
		printf("\t12 Least Number of Denominations\n");
		printf("\t13 Sum of two numbers\n");
		printf("\t14 Decimal number converter\n");
		printf("\t15 Sorting numbers\n");
		printf("\t16 Intersection of circle\n");
		printf("\t17 Smallest Circle\n");
		printf("\t18 Keypad\n");
		printf("\t19 Break time\n");
		printf("\t20 Word frequency\n");
		printf("\t21 Exit Program\n");
		printf("Choice: ");
		int ans;
		scanf("%i",&ans);
		printf("\n\n\n");
		switch(ans){
			case 1:
				num1();
				break;
			case 2:
				num2();
				break;
			case 3:
				num3();
				break;
			case 4:
				printf("Unfinished Business! :(\n");
				break;
			case 5:
				num5();
				break;
			case 6:
				num6();
				break;
			case 7:
				num7();
				break;
			case 8:
				num8();
				break;
			case 9:
				num9();
				break;
			case 10:
				num10();
				break;
			case 11:
				num11();
				break;
			case 12:
				num12();
				break;
			case 13:
				getchar();
				num13();
				break;
			case 14:
				num14();
				break;
			case 15:
				numm15();
				break;
			case 16:
				getchar();
				num16();
				break;
			case 17:
				getchar();
				num17();
				break;
			case 18:
				num18();
				break;
			case 19:
				getchar();
				num19();
				break;
			case 20:
				getchar();
				num20();
				break;
			case 21:
				return 0;
				break;
			default:
				return 0;
				break;
		}
	}
	return 0;
}