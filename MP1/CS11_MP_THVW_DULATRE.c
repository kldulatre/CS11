/*
DULATRE,KENJIE LLOYD
2014-28334
Section: CS11-THVW
Sorted Rational Roots of a Univariate Polynomial
*/

#include <stdio.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define wspace scanf("%*[^\n]")

void factors(double collector[10][100],int num,int y){
	/*
	 Get the factors of num
	 and populate the layer of collector[y][~]
	*/
	int index=0,x;
	for(x=1;x<=sqrt(num);x++){
		if(num%x==0){
			collector[y][index]=x;
			index++;
			if(x!=(num/x)){
				collector[y][index]=num/x;
				index++;
			}
		}
	}
}

void distribute(double collector[10][100]){
	int x,y,index=0;
	/*
	 Pairs the factors of the layer collector[2][~]to collector[3][~],
	 collector[4][~] quotient of collector[2][~]/collector[3][~],
	 collector[6][~] gets the value of collector[2][~],
	 collector[7][~] gets the value of collector[2][~],
	*/
	for(y=0;collector[3][y]!=0;y++){
		for(x=0;collector[2][x]!=0;x++){
			collector[4][index]=collector[2][x]/collector[3][y];
			collector[6][index]=collector[2][x];
			collector[7][index]=collector[3][y];
			index++;
		}
	}
}

void sortHighLow(double collector[10][100]){
	int x,y,index=0,limit=0,save_index;
	float max;
	/*
	 Generate the negative counterpart of the possible roots
	 and sort them into highest to lowest
	 and put them to appropriate collector layers for storage.
	*/
	for(x=0;collector[4][x]!=0;x++) limit++;

	for(x=0;x<=limit;x++){
		for(y=0,max=0;y<=limit;y++){
			if(max<collector[4][y]){
				max = collector[4][y];
				save_index = y;
			}		
		}
		collector[5][index]=max;
		collector[2][index]=collector[6][save_index];
		collector[3][index]=collector[7][save_index];
		index++;
		collector[5][index]=-max;
		collector[2][index]=-collector[6][save_index];
		collector[3][index]=collector[7][save_index];
		if(collector[5][index-1]==collector[5][index]) index=index-2;
		collector[4][save_index] = 0;
		index++;
	}
}

void sortLowHigh(double collector[10][100],int limit){
	int index=0,correct;
	float temp;
	while(true){//sort the answer form lowest to answer
		if(index=limit-1){
			index = 0;
			correct = 0;
		}else;

		int a = collector[8][index];
		int b = collector[8][index+1];
		if(collector[2][a]/collector[3][a]>collector[2][b]/collector[3][b]){
			temp = collector[8][index+1];
			collector[8][index+1] = collector[8][index];
			collector[8][index] = temp;
		}
		else correct++;
	
		index++;
		if(correct==limit-1) break;
	}
	
	for(index=0;index<limit;index++){//loop that display the sorted roots
		int a = collector[8][index];
		int b = collector[8][index-1];
		if((collector[2][b]<0) && (collector[2][a]>0) && (collector[1][0]==0)){//checks if zero is a root
			printf("0,");	
		} 
		printf("%.0f/%.0f,",collector[2][a],collector[3][a]);
	}
}

void checker(double collector[10][100],int limit){
	float answer=0;
	int x,y,z=0;

	/*
	 Isolate the roots from the array of possible roots
	 if there is only one root > immediately display the root
	 if there is none > display appropriate message
	 otherwise if will pass the roots to sortLowHigh() with the numbers of root
	*/

	for(y=0;collector[5][y]!=0;y++){
		for(x=0,answer=0;x<=limit;x++){
			answer = answer + collector[1][x]*pow(collector[5][y],x);
		}
		
		if(answer<0) answer = -answer;
		else;
		
		if(floor(answer*pow(10,6))<=1){
			collector[8][z] = y;
			z++;
		}
	}

	if(z==1){
		int a = collector[8][0];
		printf("\n\x1b[32mThe rational roots of the input polynomial are: \n\x1b[0m");
		printf("%.0f/%.0f",collector[2][a],collector[3][a]);
	}
	else if(z==0){
		printf("\nThe input polynomial has no rational roots.");
	}
	else{
		printf("\n\x1b[32mThe rational roots of the input polynomial are: \n\x1b[0m");
		sortLowHigh(collector,z);
	}
}

int main(void){
	double collector[10][100];//this is the main variable cuz it holds the whole process
	char container[100],answer[10],degree[30],temp[20];
	int x=0,y=0,counter=0,rx,ry;
	
	while(true){//this loop will continue until the user wish end the program by not by inputting 'yes'
		system("clear");//code that will clear the screen
		for(rx=0;rx<10;rx++){ //loop that will reset the collector
			for(ry=0;ry<100;ry++) collector[rx][ry] = 0;
		}
		printf("\x1b[32mEnter the highest degree of the input polynomial: \x1b[0m");
		while(true){
			/*
			 This loop will continue ask the user to input positive integer
			 Otherwise, it will display appropriate error msg
			*/
			scanf("%s",degree);
			wspace;

			if(atoi(degree)<0){
				system("clear");
				printf("\x1b[31mPlease input a positive integer!\x1b[0m\n");
				printf("\x1b[32m\nEnter the highest degree of the input polynomial: \x1b[0m");
				continue;
			}
			else if(strcmp(degree,"0")==0){
				system("clear");
				printf("\x1b[31mZero is neither a positive or negative integer!\x1b[0m\n");
				printf("\x1b[31mPlease input a positive integer!\x1b[0m\n");
				printf("\x1b[32m\nEnter the highest degree of the input polynomial: \x1b[0m");
				continue;
			}
			else if(atoi(degree)==0){
				system("clear");
				printf("\x1b[31mCharacters are never been positive integer at all!\x1b[0m\n");
				printf("\x1b[31mPlease input a positive integer!\x1b[0m\n");
				printf("\x1b[32m\nEnter the highest degree of the input polynomial: \x1b[0m");
				continue;
			}
			break;
		}

		printf("\n\x1b[32mEnter %i integer coefficients starting from the 0th degree.\x1b[0m\n",atoi(degree)+1);
		printf("\x1b[32mSeparate each input by a comma: \x1b[0m");
		while(true){
			/*
			 This loop will continue ask the user to enter coefficients properly
			 and store the coefficients in the array collector[1]
			 Otherwise, it will display appropriate error msg
			*/
			scanf("%s",container);
			wspace;
			int index,pass=0;
			for(index=0,counter=0,pass=0,y=0;index<=strlen(container);index++){
				switch(container[index]){
					case '1':
					case '2':
					case '3':
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
					case '9':
					case '0':
					case '-':
						temp[x] = container[index];
						x++;
						continue;
					case ',':
					case '\0':
						if(atoi(temp)!=0||temp[0]=='0'){
							temp[x]='\0';
							x=0;
							counter++;
							collector[1][y]=atoi(temp);
							y++;
							temp[x]='k';
							continue;
						}
						else{
							pass=1;
							break;
						}
					default:
						pass=1;
						break;	
				}
			}
			if(pass==1||counter!=atoi(degree)+1){
				printf("\x1b[31m\nPlease input properly!\n\x1b[0m");
				printf("\x1b[32mEnter %i integer coefficients starting from the 0th degree.\n\x1b[0m",atoi(degree)+1);
				printf("\x1b[32mSeparate each input by a comma: \x1b[0m");
				continue;
			}
			break;
		}
		
		int z=0;
		while(true){//checks if the first coefficient entered is 0
			if(collector[1][z]==0){
				z++;
				continue;
			}
			else break;
		}

		factors(collector,abs(collector[1][z]),2);
		factors(collector,abs(collector[1][atoi(degree)]),3);
		distribute(collector);
		sortHighLow(collector);
		
		checker(collector,atoi(degree));

		printf("\n\n\x1b[32mInput new polynomial? \x1b[0m");
		scanf("%s",answer);
		wspace;

		int a;
		for(a=0;a<strlen(answer);a++) answer[a] = toupper(answer[a]);//converts each letter in answer to Capital Letter 

		if(strcmp(answer,"YES")==0) continue;
		else break; //exit the while loop if others the user input other strings other than YES
	}

	system("clear");
	printf("\x1b[32mBYE!\x1b[0m\n");
	return 0;
}