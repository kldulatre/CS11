/*DULATRE, KENJIE LLOYD;2014-28334;THVW*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

struct record{
	int id; // holds the song id, randomly generated 
	char title[50]; //holds the title, cannot be emptied 
	char artist[50]; //holds the artist's name, can be emptied 
	char composer[50]; //holds the composer's name, can be emptied
	char album[50]; //holds the album's title
	char genre[50]; //holds the genre of the song 
	int rating; // hold the user define rating inclusive from 1 to 5 
	char remarks[50]; //holds notes of the user to the song 
};
void addSong(void){
/*
Display the screen that helps to add a song to the whole library 
Topmost is the banner  
Followed by the generated Song Id 
Then Title, Artist, Composer, Album, Genre, Rating, Remarks 
Continue asking for Title if user emptied it 
Continue asking if Rating is inclusive between 1 and 5 
Save the new record the file record.txt by appending 
Lastly, ask the user if the he/she still wishes to continue adding new record 
If not, go to the navigation page 
*/
	int pass = 0;

	while(true){
		system("clear");
		
		FILE *file;
		struct record *dummy;
		file = fopen("records.txt","a");

		dummy = (struct record *)malloc(sizeof(struct record));
		
		dummy->id = rand();
		printf("\x1b[32mMusic Record Library\x1b[0m\n\x1b[32m of Yvonne Murelle\x1b[0m\n\n");
		printf("\x1b[33mSong ID:\x1b[0m %i\n",dummy->id);
		
		while(true){
			printf("\x1b[33mTitle:\x1b[0m ");	
			fgets(dummy->title,50,stdin);
			dummy->title[strlen(dummy->title)-1] = '\0';
			if(strcmp(dummy->title,"")==0) continue;
			else break;
		}

		printf("\x1b[33mArtist:\x1b[0m ");
		fgets(dummy->artist,50,stdin);
		dummy->artist[strlen(dummy->artist)-1] = '\0';

		printf("\x1b[33mComposer:\x1b[0m ");
		fgets(dummy->composer,50,stdin);
		dummy->composer[strlen(dummy->composer)-1] = '\0';
		
		printf("\x1b[33mAlbum:\x1b[0m ");
		fgets(dummy->album,50,stdin);
		dummy->album[strlen(dummy->album)-1] = '\0';
		
		printf("\x1b[33mGenre:\x1b[0m ");
		fgets(dummy->genre,50,stdin);
		dummy->genre[strlen(dummy->genre)-1] = '\0';

		while(true){
			printf("\x1b[33mRating:\x1b[0m ");
			char tempRate[10];
			fgets(tempRate,10,stdin);
			tempRate[strlen(tempRate)-1]='\0';
			if(strcmp(tempRate,"")==0) continue;
			else if(atoi(tempRate)>=1&&atoi(tempRate)<=5){
				dummy->rating = atoi(tempRate);
				break;
			}
			else continue;
		}

		printf("\x1b[33mRemarks:\x1b[0m ");
		fgets(dummy->remarks,50,stdin);
		dummy->remarks[strlen(dummy->remarks)-1] = '\0';

		fwrite(&dummy->id, sizeof(dummy->id), 1, file);
	    fwrite(dummy->title, 50, 1, file);
	    fwrite(dummy->artist, 50, 1, file);
	    fwrite(dummy->composer, 50, 1, file);
	    fwrite(dummy->album, 50, 1, file);
	    fwrite(dummy->genre, 50, 1, file);
	    fwrite(&dummy->rating, sizeof(dummy->id), 1, file);
	    fwrite(dummy->remarks, 50, 1, file);

	    fclose(file);
	    free(dummy);

	    char c;
	    printf("\x1b[33m\n\nAdd another one song(y/n)? \x1b[0m");
   		scanf("%c",&c);
   		if(c=='y'||c=='Y') pass = 0;
   		else pass = 1;
   		if(pass == 1){
   			system("clear");
   			break;
   		}
   		getchar();
   	}
}

void listSong(int filter,char *ptr){
/*
Opens the file record.txt and reads the file by every song record. 
Accept two(2) parameters namely filter and ptr 
filter use to direct to the proper field to search the substring 
ptr pointer to the substring 
Render all the information if the substring match to the chosen field except for the Song ID 
If the user choose the display all the songs the Song ID would also be displayed 
*/
	FILE *file;
	struct record *dummy;
	file = fopen("records.txt","rb");
	if (file == NULL){    
        printf("\nThere is no Song Library Yet!\nCreate One First");
    }
    else{
		dummy = (struct record *)malloc(sizeof(struct record));

		while(fread(&dummy->id, sizeof(dummy->id), 1, file)){
	        fread(dummy->title, 50, 1, file);
			fread(dummy->artist, 50, 1, file);
		    fread(dummy->composer, 50, 1, file);
		    fread(dummy->album, 50, 1, file);
		    fread(dummy->genre, 50, 1, file);
		    fread(&dummy->rating, sizeof(dummy->rating), 1, file);
		    fread(dummy->remarks, 50, 1, file);
			// printf("number %i",atoi(ptr));
			if((filter==1)&&(strstr(dummy->title,ptr))){
				printf("\n\x1b[33mTitle:\x1b[0m %s\n",dummy->title);
				printf("\x1b[33mArtist:\x1b[0m %s\n",dummy->artist);
				printf("\x1b[33mComposer:\x1b[0m %s\n",dummy->composer);
				printf("\x1b[33mAlbum:\x1b[0m %s\n",dummy->album);
				printf("\x1b[33mGenre:\x1b[0m %s\n",dummy->genre);
				printf("\x1b[33mRating:\x1b[0m %d\n",dummy->rating);
				printf("\x1b[33mRemarks:\x1b[0m %s\n",dummy->remarks);
			}
			else if((filter==2)&&(strstr(dummy->artist,ptr))){
				printf("\n\x1b[33mTitle:\x1b[0m %s\n",dummy->title);
				printf("\x1b[33mArtist:\x1b[0m %s\n",dummy->artist);
				printf("\x1b[33mComposer:\x1b[0m %s\n",dummy->composer);
				printf("\x1b[33mAlbum:\x1b[0m %s\n",dummy->album);
				printf("\x1b[33mGenre:\x1b[0m %s\n",dummy->genre);
				printf("\x1b[33mRating:\x1b[0m %d\n",dummy->rating);
				printf("\x1b[33mRemarks:\x1b[0m %s\n",dummy->remarks);
			}
			else if((filter==3)&&(strstr(dummy->composer,ptr))){
				printf("\n\x1b[33mTitle:\x1b[0m %s\n",dummy->title);
				printf("\x1b[33mArtist:\x1b[0m %s\n",dummy->artist);
				printf("\x1b[33mComposer:\x1b[0m %s\n",dummy->composer);
				printf("\x1b[33mAlbum:\x1b[0m %s\n",dummy->album);
				printf("\x1b[33mGenre:\x1b[0m %s\n",dummy->genre);
				printf("\x1b[33mRating:\x1b[0m %d\n",dummy->rating);
				printf("\x1b[33mRemarks:\x1b[0m %s\n",dummy->remarks);
			}
			else if((filter==4)&&(strstr(dummy->album,ptr))){
				printf("\n\x1b[33mTitle:\x1b[0m %s\n",dummy->title);
				printf("\x1b[33mArtist:\x1b[0m %s\n",dummy->artist);
				printf("\x1b[33mComposer:\x1b[0m %s\n",dummy->composer);
				printf("\x1b[33mAlbum:\x1b[0m %s\n",dummy->album);
				printf("\x1b[33mGenre:\x1b[0m %s\n",dummy->genre);
				printf("\x1b[33mRating:\x1b[0m %d\n",dummy->rating);
				printf("\x1b[33mRemarks:\x1b[0m %s\n",dummy->remarks);
			}
			else if((filter==5)&&(strstr(dummy->genre,ptr))){
				printf("\n\x1b[33mTitle:\x1b[0m %s\n",dummy->title);
				printf("\x1b[33mArtist:\x1b[0m %s\n",dummy->artist);
				printf("\x1b[33mComposer:\x1b[0m %s\n",dummy->composer);
				printf("\x1b[33mAlbum:\x1b[0m %s\n",dummy->album);
				printf("\x1b[33mGenre:\x1b[0m %s\n",dummy->genre);
				printf("\x1b[33mRating:\x1b[0m %d\n",dummy->rating);
				printf("\x1b[33mRemarks:\x1b[0m %s\n",dummy->remarks);
			}
			else if((filter==6)&&(dummy->rating>=atoi(ptr))){
				printf("\n\x1b[33mTitle:\x1b[0m %s\n",dummy->title);
				printf("\x1b[33mArtist:\x1b[0m %s\n",dummy->artist);
				printf("\x1b[33mComposer:\x1b[0m %s\n",dummy->composer);
				printf("\x1b[33mAlbum:\x1b[0m %s\n",dummy->album);
				printf("\x1b[33mGenre:\x1b[0m %s\n",dummy->genre);
				printf("\x1b[33mRating:\x1b[0m %d\n",dummy->rating);
				printf("\x1b[33mRemarks:\x1b[0m %s\n",dummy->remarks);
			}
			else if(filter==7){
				printf("\n\x1b[33mSong Id:\x1b[0m %d\n",dummy->id);
				printf("\x1b[33mTitle:\x1b[0m %s\n",dummy->title);
				printf("\x1b[33mArtist:\x1b[0m %s\n",dummy->artist);
				printf("\x1b[33mComposer:\x1b[0m %s\n",dummy->composer);
				printf("\x1b[33mAlbum:\x1b[0m %s\n",dummy->album);
				printf("\x1b[33mGenre:\x1b[0m %s\n",dummy->genre);
				printf("\x1b[33mRating:\x1b[0m %d\n",dummy->rating);
				printf("\x1b[33mRemarks:\x1b[0m %s\n",dummy->remarks);
			}
		}
	}
	free(dummy);
	fclose(file);

}
void listSongChannel(void){
/*
Gateway for the listSong function after analyzing and processing the given inputted query by the user. 
Display the possible choice of query to be inputted the user. 
The query is divided into two, token and string 
The token is choosing the right field to where to search the substring 
The string holds the substring 
The listSongChannel passes two parameters to listSong 
filter contain the equivalent for the given field 
string holds the substring to pass 
Ask the user at the end if he/she still want to continue searching 
If not navigate to navigation screen
*/
	char *token=NULL;
    int pass = 1;
    char str[50];
	while(true){
		system("clear");
		printf("\x1b[32mMusic Record Library\x1b[0m\n\x1b[32m of Yvonne Murelle\x1b[0m\n\n");
		printf("\x1b[33mPossible Query: \x1b[0m\n");
		printf("   \x1b[34mTitle <query>\x1b[0m\n");
		printf("   \x1b[34mArtist <query>\x1b[0m\n");
	    printf("   \x1b[34mComposer <query>\x1b[0m\n");
	    printf("   \x1b[34mAlbum <query>\x1b[0m\n");
	    printf("   \x1b[34mGenre <query>\x1b[0m\n");
	    printf("   \x1b[34mRating <query>\x1b[0m\n");
	    printf("   \x1b[34mAll\x1b[0m\n\n");
	    printf("\x1b[33mEnter your query:\x1b[0m ");
	    fgets(str,50,stdin);
	    
	    if(strcmp(str,"")==0) continue;
	    else str[strlen(str)-1] = '\0';
	    
	    char *string = str;
	    token = strsep(&string," ");
		int a;

	    for(a=0;a<strlen(token);a++) token[a] = toupper(token[a]);
	    if(strcmp(token,"TITLE")==0&&string!=NULL) listSong(1,string);	
		else if(strcmp(token,"ARTIST")==0&&string!=NULL) listSong(2,string);
		else if(strcmp(token,"COMPOSER")==0&&string!=NULL) listSong(3,string);
		else if(strcmp(token,"GENRE")==0&&string!=NULL) listSong(4,string);
		else if(strcmp(token,"ALBUM")==0&&string!=NULL) listSong(5,string);
		else if(strcmp(token,"RATING")==0&&string!=NULL) listSong(6,string);
		else if(strcmp(token,"ALL")==0) listSong(7,string);
		else;

		char c;
	    printf("\x1b[33m\nAnother list of songs(y/n)? \x1b[0m");
   		scanf("%c",&c);
   		if(c=='y'||c=='Y') pass = 0;
   		else pass = 1;
   		if(pass == 1){
   			system("clear");
   			break;
   		}
   		getchar();
	}
}
void updateSong(){
/*
Display the screen that helps the user to update a song 
The song could be choose by inputting the title after the screen was render 
The screen also follows the convention, banner at the topmost 
If the song's title inputted matches a record get the record every information and display it to the user one by one as guide for the user for updating 
The user may not wish to update a information by leaving the entry field blank. 
After getting the updated informations save the informations/data to the file records.txt 
At the end ask the user if the he/she still wants to continue updating 
If not navigate to the navigation screen
*/
	FILE *file;
	int pass = 0;
	while(true){
		struct record *dummy,*dummyUpdate;
		file = fopen("records.txt","r+");

		if (file == NULL){    
	        printf("\nThere is no Song Library Yet!\nCreate One First");
	    }
	    else{
	    	char searching[50];
	    	dummy = (struct record *)malloc(sizeof(struct record));
	    	system("clear");
			printf("\x1b[32mMusic Record Library\x1b[0m\n\x1b[32m of Yvonne Murelle\x1b[0m\n\n");
	    	printf("\x1b[33mSearch Song: \x1b[0m");
			fgets(searching,50,stdin);

			if(strcmp(searching,"")==0) printf("Nothing to search");
		    else searching[strlen(searching)-1] = '\0';

	    	while(fread(&dummy->id, sizeof(dummy->id), 1, file)){
		        fread(dummy->title, 50, 1, file);
				fread(dummy->artist, 50, 1, file);
			    fread(dummy->composer, 50, 1, file);
			    fread(dummy->album, 50, 1, file);
			    fread(dummy->genre, 50, 1, file);
			    fread(&dummy->rating, sizeof(dummy->rating), 1, file);
			    fread(dummy->remarks, 50, 1, file);
				
				// if(strcmp(dummy->title,searching)==0){
				if(strstr(dummy->title,searching)){
					dummyUpdate = (struct record *)malloc(sizeof(struct record));
					printf("\x1b[33mTitle\x1b[0m \x1b[34m(%s)\x1b[0m\x1b[33m:\x1b[0m ",dummy->title);
					fgets(dummyUpdate->title,50,stdin);
					dummyUpdate->title[strlen(dummyUpdate->title)-1]='\0';
					printf("\x1b[33mArtist\x1b[0m \x1b[34m(%s)\x1b[0m\x1b[33m:\x1b[0m ",dummy->artist);
					fgets(dummyUpdate->artist,50,stdin);
					dummyUpdate->artist[strlen(dummyUpdate->artist)-1]='\0';
					printf("\x1b[33mComposer\x1b[0m \x1b[34m(%s)\x1b[0m\x1b[33m:\x1b[0m ",dummy->composer);
					fgets(dummyUpdate->composer,50,stdin);
					dummyUpdate->composer[strlen(dummyUpdate->composer)-1]='\0';
					printf("\x1b[33mAlbum\x1b[0m \x1b[34m(%s)\x1b[0m\x1b[33m:\x1b[0m ",dummy->album);
					fgets(dummyUpdate->album,50,stdin);
					dummyUpdate->album[strlen(dummyUpdate->album)-1]='\0';
					printf("\x1b[33mGenre\x1b[0m \x1b[34m(%s)\x1b[0m\x1b[33m:\x1b[0m ",dummy->genre);
					fgets(dummyUpdate->genre,50,stdin);
					dummyUpdate->genre[strlen(dummyUpdate->genre)-1]='\0';
					while(true){
						printf("\x1b[33mRating\x1b[0m \x1b[34m(%i)\x1b[0m\x1b[33m:\x1b[0m ",dummy->rating);
						char tempRate[10];
						fgets(tempRate,10,stdin);
						tempRate[strlen(tempRate)-1]='\0';
						if(strcmp(tempRate,"")==0){
							dummyUpdate->rating = dummy->rating;
							break;
						}
						else if(atoi(tempRate)<1&&atoi(tempRate)>5) continue;
						else{ 
							dummyUpdate->rating = atoi(tempRate);
							break;
						}
					}
					printf("\x1b[33mRemarks\x1b[0m \x1b[34m(%s)\x1b[0m\x1b[33m:\x1b[0m ",dummy->remarks);
					fgets(dummyUpdate->remarks,50,stdin);
					dummyUpdate->remarks[strlen(dummyUpdate->remarks)-1]='\0';

					fseek(file, -308, SEEK_CUR);
		            fwrite(&dummy->id, sizeof(dummy->id), 1, file);
				   
				    if(strcmp(dummyUpdate->title,"")!=0) fwrite(dummyUpdate->title, 50, 1, file);
				    else fwrite(dummy->title, 50, 1, file);
				    if(strcmp(dummyUpdate->artist,"")!=0) fwrite(dummyUpdate->artist, 50, 1, file);
				    else fwrite(dummy->artist, 50, 1, file);
				    if(strcmp(dummyUpdate->composer,"")!=0) fwrite(dummyUpdate->composer, 50, 1, file);
				    else fwrite(dummy->composer, 50, 1, file);
				    if(strcmp(dummyUpdate->album,"")!=0) fwrite(dummyUpdate->album, 50, 1, file);
				    else fwrite(dummy->album, 50, 1, file);
				    if(strcmp(dummyUpdate->genre,"")!=0) fwrite(dummyUpdate->genre, 50, 1, file);
				    else fwrite(dummy->genre, 50, 1, file);
				    fwrite(&dummyUpdate->rating, sizeof(dummyUpdate->rating), 1, file);
				    if(strcmp(dummyUpdate->remarks,"")!=0) fwrite(dummyUpdate->remarks, 50, 1, file);
				    else fwrite(dummy->remarks, 50, 1, file);

		            break;
				}
			}
	    }
	    free(dummy);
	    fclose(file);

	    char c;
	    printf("\x1b[33m\nUpdate again(y/n)? \x1b[0m");
   		scanf("%c",&c);
   		if(c=='y'||c=='Y') pass = 0;
   		else pass = 1;
   		if(pass == 1){
   			system("clear");
   			break;
   		}
   		getchar();
	}
}
int main(void){
/*
Display the navigation screen 
Topmost is the banner 
Serve the 3 group command as menu 
The user picks the assign number for the command to navigate to the command screen 
Number 4 to exit/terminate the program
*/
	while(true){
        int pass=1,choice;
		system("clear");
		printf("\x1b[32mMusic Record Library\x1b[0m\n\x1b[32m of Yvonne Murelle\x1b[0m\n\n");
        printf("\x1b[33mChoose what to do: \x1b[0m\n");
        printf("   \x1b[34m1] Add a Song\x1b[0m\n");
	    printf("   \x1b[34m2] List Songs\x1b[0m\n");
	    printf("   \x1b[34m3] Update a Song record\x1b[0m\n");
	    printf("   \x1b[34m4] Exit\x1b[0m\n");
        printf("\n\x1b[33mEnter your choice: \x1b[0m");
        scanf("%d", &choice);
        switch (choice){
	        case 1:
	        	getchar();
		        addSong();
	            break;
	        case 2:
	        	getchar();
	        	listSongChannel();
				break;
	        case 3:
	        	getchar();
	        	updateSong();    
	            break;
	        case 4: 
	            return 0;
        }
        getchar();
    }
}